const members = [
    {
        "id": 1,
        "name": "Yashi",
        "email": "yashi.choksi@dhiwise.com"  
    },
    {
        "id": 2,
        "name": "Priyanshi",
        "email": "priyanshi.jari@dhiwise.com"  
    },
    {
        "id": 3,
        "name": "Rutva",
        "email": "rutva.joshi@dhiwise.com"  
    },
    {
        "id": 4,
        "name": "Priyanka",
        "email": "priyanka@dhiwise.com"  
    }
];


module.exports = members;