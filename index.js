const express = require('express');
const path = require('path');
// const members = require('./members');
const logger = require('./middleware/logger');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended : false}));


// app.get('/',(req, res) => {
//     res.sendFile(path.join(__dirname,'public','index.html'));
// });
app.use(express.static(path.join(__dirname,'public')));

app.use('/api/members',require('./routes/api/Members'));

// app.use(logger);

// app.get('/api/members',(req,res) => {
//     res.json(members);
// });


// app.get('/api/members/:id', (req, res) => {
//     // console.log(req.params.id);
//     const id = parseInt(req.params.id);
//     const found = members.some(member => members.id === id);

//     if(found){
//         res.json(members.filter(members => members.id === id));
//     }else{
//         res.status(400).json({msg : `Member not found of id ${id}`});
//     }
    
// });

const PORT = 5000;

app.listen(PORT, () => console.log(`server starts on port ${PORT}`));
