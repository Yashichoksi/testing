const express = require('express');
const router = express.Router();
const members = require('../../members');
const uuid = require('uuid');

// select all
router.get('/',(req,res) => {
    res.json(members);
});

// select by id
 router.get('/:id', (req, res) => {
    //  console.log(req.params.id);
     const id = parseInt(req.params.id);
     const found = members.some(members => members.id === id);
     console.log(found);
     console.log(members.filter(members => members.id === id));

     if(found){
         res.json(members.filter(members => members.id === id));
     }else{
         res.status(400).json({msg : `Member not found of id ${id}`});
     }
    
});

// insert single
router.post('/',(req,res) => {
    const newMember = {
        id:uuid.v4(),
        name : req.body.name,
        email: req.body.email
    }

    if(!req.body.name || !req.body.email){
        return res.status(400).json({ msg: 'Please add name and email.!'});
    }
    // else{
        // res.json(newMember);
    // }
    members.push(newMember);
    res.json(members);

});


//update data

router.put('/:id', (req, res) => {
    //  console.log(req.params.id);
     const id = parseInt(req.params.id);
     const found = members.some(members => members.id === id);
     console.log(found);
     console.log(members.filter(members => members.id === id));

     if(found){
         const updMember = req.body;
         members.forEach(members => {
             if(members.id = id){
                 members.name = updMember.name ? updMember.name : req.body.name;
                 members.email = updMember.email ? updMember.email : req.body.email;

                 res.json({ msg: 'Members updated succesfully.!',members });
             }
         });
     }else{
         res.status(400).json({msg : `Member not found of id ${id}`});
     }
    
});


// delete by id
router.delete('/:id', (req, res) => {
    //  console.log(req.params.id);
     const id = parseInt(req.params.id);
     const found = members.some(members => members.id === id);
     console.log(found);
     console.log(members.filter(members => members.id === id));

     if(found){
         res.json({
             msg : 'Member Deleted Successfully.!',
             members : members.filter(members => members.id !== id)
        });
     }else{
         res.status(400).json({msg : `Member not found of id ${id}`});
     }
    
});
module.exports = router;